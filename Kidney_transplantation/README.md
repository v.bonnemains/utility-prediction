KiTRUS: a tool for predicting Kidney-Transplant Recipients longitudinal Utility Scores
================

## Description

KiTRUS is an online calculator predicting kidney transplant (KT) recipients utility scores over time from clinical characteristics.
It enables the estimation of Quality Adjusted Life Years (QALYs) from most KT clinical databases when no longitudinal preference-based HRQol measures collection were collected.

Predictions are performed using either a linear mixed model or a two-part beta mixed model. Please, refer to the original publication for comprehensive description of the methodology.

Click [here](https://jeo8i7-vincent-bonnemains.shinyapps.io/KiTRUS/) to access the online calculator.\
R users can also perform the predictions inside R by following the instructions below.

## Input

The dataset from which to predict utility scores should contain :

* a variable identifying the recipient, in a column named 'id'
* the recipients age in years, in a column named 'age'
* whether the recipients are male (1 if so, 0 otherwise), in a column named 'sex'
* whether the recipients' BMI is greater than 30kg/m² (1 if so, 0 otherwise), in a column named 'bmi'
* whether the recipients have diabetes history (1 if so, 0 otherwise), in a column named 'diabetes'
* whether the recipients have cardiovascular history (1 if so, 0 otherwise), in a column named 'cv'
* time spent on dialysis by the recipients prior to transplantation in years, in a column named 'timedial'
* time at which utility scores should be predicted (i.e. time since transplant, in years), in a column named 'time'

Every column is required for utility prediction. The dataset may contain other columns of any format. In the event that you receive an error, please check that the name and format of the required variables comply with the instructions above.

The prediction functions below offer the possibility to set a minimal possible utility value.
The minimal EQ5D utility score depends on the respondent's country (see https://euroqol.org/information-and-support/resources/value-sets/).
The default value u_min = -0.53 corresponds to the minimal utility score in the French cohort on which these models were estimated

## Choice of the predicting model

Two regression models are proposed to perform the predictions: a linear mixed effect model (LME) and a two-part beta mixed effect model. In the study supporting this tool, the two-part beta mixed model resulted in the lowest root mean squared errors (RMSE), but was poorly calibrated. Indeed, it overestimated low utility values and underestimated high ones. Therefore its use would result in biased estimation, should it be applied to a population at either side of the scale. On the other hand, the LME was the best calibrated model and should be preferred in most cases.

## Basic usage
``` r

# Load functions and models

source("https://gitlab.com/v.bonnemains/utility-prediction/-/raw/main/Kidney_transplantation/utility%20prediction.R")
load(url("https://gitlab.com/v.bonnemains/utility-prediction/-/raw/main/Kidney_transplantation/regression%20models.RData"))

# Dummy dataset

ex_data <- data.frame(id=c(rep(1,3), rep(2,3)),
                      age=c(rep(20,3), rep(70,3)),
                      sex=c(rep(1,3), rep(0,3)),
                      bmi=c(rep(0,3), rep(1,3)),
                      diabetes=c(rep(0,3), rep(1,3)),
                      cv=c(rep(0,3), rep(1,3)),
                      timedial=c(rep(0,3), rep(7.5,3)),
                      time=rep(c(0, 1, 10), 2))

 # The functions returns the original dataset with an additional column containing utility predictions
 
predict.lme(ex_data) # predictions using the LME

#   id time utility_scores age sex bmi diabetes cv timedial
# 1  1    0          0.907  20   1   0        0  0      0.0
# 2  1    1          0.971  20   1   0        0  0      0.0
# 3  1   10          0.925  20   1   0        0  0      0.0
# 4  2    0          0.517  70   0   1        1  1      7.5
# 5  2    1          0.581  70   0   1        1  1      7.5
# 6  2   10          0.535  70   0   1        1  1      7.5

predict.2part_beta(ex_data) # predictions using the 2-part beta model

#   id time utility_scores age sex bmi diabetes cv timedial
# 1  1    0          0.906  20   1   0        0  0      0.0
# 2  1    1          0.960  20   1   0        0  0      0.0
# 3  1   10          0.930  20   1   0        0  0      0.0
# 4  2    0          0.445  70   0   1        1  1      7.5
# 5  2    1          0.538  70   0   1        1  1      7.5
# 6  2   10          0.467  70   0   1        1  1      7.5

predict.2part_beta(ex_data, u_min=-0.217) # predictions using the 2-part beta model, with the Australian minimal EQ-5D-3L utility value 

#   id time utility_scores age sex bmi diabetes cv timedial
# 1  1    0          0.925  20   1   0        0  0      0.0
# 2  1    1          0.968  20   1   0        0  0      0.0
# 3  1   10          0.945  20   1   0        0  0      0.0
# 4  2    0          0.559  70   0   1        1  1      7.5
# 5  2    1          0.632  70   0   1        1  1      7.5
# 6  2   10          0.576  70   0   1        1  1      7.5

```

## Interpretation

The predictions obtained through these models only apply to kidney transplant recipients **alive, with a functioning graft**. Inproper use could result in [survival bias](https://en.wikipedia.org/wiki/Survivorship_bias) due to the fact that higher utility values may be associated with higher patient and graft survival.

## Reporting bugs

You can report any issues at this [address](mailto:contact-project+v-bonnemains-utility-prediction-57403674-issue-@incoming.gitlab.com).
